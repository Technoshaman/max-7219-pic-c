#ifndef __MAX7219_H
#define __MAX7219_H

#define NOOP 0x00
#define DIGIT0 0x01
#define DIGIT1 0x02
#define DIGIT2 0x03
#define DIGIT3 0x04
#define DIGIT4 0x05
#define DIGIT5 0x06
#define DIGIT6 0x07
#define DIGIT7 0x08
#define DECODEMODE 0x09
#define INTENSITY 0x0A
#define SCANLIMIT 0x0B
#define SHUTDOWN 0x0C
#define DISPLAYTEST 0x0F

//         Char        Seg Val                 ASC Hex                 ASC Dec
//------------------------------------------------------------------------------------
//          BLK          0x00                    0x20                    32
//           -           0x01                    0x2D                    45
//           .           0x80                    0x2E                    46
//          ZRO          0x7E                    0x30                    48
//          ONE          0x30                    0x31                    49
//          TWO          0x6D         		 	 0x32                    50
//          THR          0x79                    0x33                    51
//          FOR          0x33                    0x34                    52
//          FIV          0x5B                    0x35                    53
//          SIX          0x5F                    0x36                    54
//          SEV          0x70                    0x37                    55
//          EGT          0x7F                    0x38                    56
//          NIN          0x73          		 	 0x39                    57
//          A            0x77                    0x41                    65
//          C            0x4E                    0x43                    67
//          E            0x4F                    0x45                    69
//          F            0x47                    0x46                    70
//          G            0x5E          		 	 0x47                    71
//          H            0x37          		 	 0x48                    72
//          J            0x3C          		 	 0x4A                    74
//          L            0x0E          		 	 0x4C                    76
//          P            0x67         		 	 0x50                    80
//          Q            0xFE         		 	 0x51                    81
//          U            0x3E          		 	 0x55                    85
//          a            0x9D                  	 0x61  		         	 97
//          b            0x1F                    0x62              	 	 98
//          c            0x0D                    0x63                    99
//          d            0x3D     		 		 0x64                    100
//          g            0x7B          		 	 0x67                    103
//          h            0x17                    0x68                    104
//          n            0x15                    0x6E                    110
//          o            0x1D                    0x6F                    111
//          r            0x05          		 	 0x72                    114
//          t            0x07                    0x74                    116
//          u            0x1C          		 	 0x75                    117
//          y            0x3B                    0x79                    121

// We need ASCII characters from decimal 32 to 121 (90 chars), Seg Val's are placed here
// We'll handle the 8 byte char array accordingly
// Increase the digit as the char array is scanned

const unsigned char sgmts_Table[90] = {
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 32 - 41 blank
0x00, 0x00, 0x00, 0x01, 0x80, 0x00, 0x7E, 0x30, 0x6D, 0x79, // 42 - 51 dash, dot, 0, 1, 2, 3
0x33, 0x5B, 0x5F, 0x70, 0x7F, 0x73, 0x00, 0x00, 0x00, 0x00, // 52 - 61 4, 5, 6, 7, 8, 9
0x00, 0x00, 0x00, 0x77, 0x00, 0x4E, 0x00, 0x4F, 0x47, 0x5E, // 62 - 71 A, C, E, F, G
0x37, 0x00, 0x3C, 0x00, 0x0E, 0x00, 0x00, 0x00, 0x67, 0xFE, // 72 - 81 H, J, L, P, Q
0x00, 0x00, 0x00, 0x3E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 82 - 91 U
0x00, 0x00, 0x00, 0x00, 0x00, 0x9D, 0x1F, 0x0D, 0x3D, 0x00, // 92 - 101 a, b, c, d,
0x00, 0x7B, 0x17, 0x00, 0x00, 0x00, 0x00, 0x00, 0x15, 0x1D, // 102 - 111 g, h, n, o
0x00, 0x00, 0x05, 0x00, 0x07, 0x1C, 0x00, 0x00, 0x00, 0x3B  // 112 - 121 r, t, u, y
};

void Setup(void); // PIC initialization function
void SPI_write(unsigned char txdata); // SPI write function
unsigned char read_ADC(unsigned char chan); // ADC read function
void set_Decode(unsigned char mode); // BCD Decode on/off
void set_Segments(unsigned char segs); // Number of segments to activate 0x00 - 0x07
void set_Brightness(unsigned char level); // Brightness level 0x00 - 0x0F
void set_Display(unsigned char flag); // Display on/off
void set_Disp_Test(unsigned char flag); // Display Test
void wrt_Segments(unsigned char sgmnt, unsigned char charc); // Write to segments 0 to 7
void wrt_Display(const char *char_arr); // Write char array to display (ASCII accepted)
void clr_Segments(void);

#endif
