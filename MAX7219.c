#ifndef __MAX7219_C
#define __MAX7219_C

#include <string.h>
#include <pic18.h>
#include <MAX7219.h>

#define CS RB0 // Chip Select for MAX7219

#define off 0
#define on 1

#define false 0
#define true 1

static unsigned char flag = false;

void Setup (void)
{
        ADCON1 = 0x4E; // AN0
        ADCS0 = 0x00; // Tad = Fosc / 64 => 2us
        ADCS1 = 0x01;
        ADON = 0x01; // ADC On

        CCP1CON = 0x00;

        TRISA = 0x01; // Initialize ports, analog input for later
        TRISB = 0x00;
        TRISC = 0x10;

        PORTA = 0x00;
        PORTB = 0x00;
        PORTC = 0x00;

        SSPSTAT = 0x40; // CKE=1 SMP=0 (0x40) - SPI Mode 0,0
        SSPCON1 = 0x20; // CKP=0 SSPEN=1 SCK=Fosc/4 (0x20) - 8MHz SPI comm. with 8Mhz XTAL using PLL (32MHz)
}

void SPI_write(unsigned char txdata)
{
        unsigned char rxdata;

        rxdata = SSPBUF; // Lose any data in buffer, clear BF
        SSPBUF = txdata; // Load transmit data
        while(!BF); // Wait until transmit complete
}

void set_Decode(unsigned char mode)
{
		CS = off; // Enable CS
        SPI_write(DECODEMODE); // Decode mode
        SPI_write(mode);
        CS = on; // Disable CS
}

void set_Segments(unsigned char segs)
{
        CS = off; // Enable CS
        SPI_write(SCANLIMIT);
        SPI_write(segs); // scan up to this segment ++ 0x00 - 0x07
        CS = on; // Disable CS
}

void set_Brightness(unsigned char level)
{
        CS = off; // Enable CS
        SPI_write(INTENSITY);
        SPI_write(level); // brightness level ++ 0x00 - 0x0F
        CS = on; // Disable CS
}

void set_Display(unsigned char flag)
{
        CS = off; // Enable CS
        SPI_write(SHUTDOWN); // display
        SPI_write(flag); // on/off
        CS = on; // Disable CS
}

void set_Disp_Test(unsigned char flag)
{
        CS = off; // Enable CS
        SPI_write(DISPLAYTEST); // test
        SPI_write(flag); // on/off
        CS = on; // Disable CS
}

void wrt_Segments(unsigned char sgmnt, unsigned char charc) // Write to segments 1 - 8
{
	static unsigned char buff_Prev = 0;

	if(charc >= 32 && charc <= 121) // Valid character?
	{
		if(charc == 0x2E) // DP?
		{
			CS = off;
			SPI_write(sgmnt - 1);
			SPI_write(sgmts_Table[buff_Prev - 32] | 0x80); 	// Add DP to previous character and redisplay
			CS = on;					// -32 since our table starts from the 32th ASCII character
			flag = true;
		}
		else
		{
			CS = off;

			if(flag) // Still true at second DP - fails to display correctly
				SPI_write(sgmnt - 1);
			else
				SPI_write(sgmnt);

			SPI_write(sgmts_Table[charc - 32]);
			CS = on;
			buff_Prev = charc; // Previous char for next round
		}
	}
	else // Ignore characters not contained in the table
	{
		CS = off;
		SPI_write(sgmnt);
		SPI_write(sgmts_Table[0x00]);
		CS = on;
	}
}

void wrt_Display(const char *char_arr) // Write char array to display
{
	unsigned int cntr = 0, len = 0;

	len = strlen(char_arr) - 1;

	for(cntr = 0; cntr <= len; ++cntr)
		wrt_Segments(cntr + 1, char_arr[cntr]);

	flag = false;
}

void clr_Segments(void)
{
	wrt_Display("        ");

}

#endif
